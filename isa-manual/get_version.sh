#!/usr/bin/env bash

TARGET_FILE=$1
TMP_FILE=$(mktemp)

# Get the version using git describe.
VERSION="$(git describe --tags --match 'v*.*' --dirty=-MODIFIED)"
echo "Version: ${VERSION}"

# Generate a \specrev TeX command based on the version.
echo '\newcommand{\specrev}{\mbox{'"${VERSION}"'}}' > "${TMP_FILE}"

# Only replace the target file if it's different from the temporary file.
diff "${TMP_FILE}" "${TARGET_FILE}" &>/dev/null || cp "${TMP_FILE}" "${TARGET_FILE}"

rm -f "${TMP_FILE}"

